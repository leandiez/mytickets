<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
     /**
     * Muestra la página de Proyectos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('project');
    }
}
