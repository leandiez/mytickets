<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
     /**
     * Muestra la página de Usuarios del sistema
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users');
    }
}
