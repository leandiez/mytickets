<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    /**
     * Muestra la página de Configuracion
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('config');
    }
}
