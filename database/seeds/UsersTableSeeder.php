<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//usuario Administrador
        User::create([
        	'name'=>'Enzo',
        	'email'=>'eantoni@batecno.com.ar',
        	'password'=>bcrypt('123456'),
        	'role'=>'0'
        ]);
        
        User::create([
        	'name'=>'Javier',
        	'email'=>'jgamberg@batecno.com.ar',
        	'password'=>bcrypt('123456'),
        	'role'=>'1'
        ]);
        User::create([
        	'name'=>'Maxi',
        	'email'=>'mlembo@batecno.com.ar',
        	'password'=>bcrypt('123456'),
        	'role'=>'2'
        ]);
    }
}
