@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Usuarios</div>

    <div class="card-body">
        <form action="">

            <div class="form-group">
                <label for="category_id">Categoría</label>
                <select class="form-control" name="category_id"></select>
            </div>
            <div class="form-group">
                <label for="severity">Severidad</label>
                <select class="form-control" name="severity">
                    <option value="B">Baja</option>
                    <option value="M">Media</option>
                    <option value="A">Alta</option>
                </select>
            </div>
            <div class="form-group">
                <label for="resumen_id">Resumen</label>
                <input class="form-control" type="text" name="resumen_id">
            </div>
            <div class="form-group">
                <label for="description_id">Descripcion</label>
                <textarea class="form-control" name="description_id"></textarea>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary">Registrar Incidencia</button>
            </div>
        </form>
    </div>
</div>
@endsection
